#!/usr/bin/env groovy

package com.example

class Docker implements Serializable{

    def script

    Docker(script) {
        this.script = script
    }

    def buildApplication(){
        script.echo "building the application...for branch $script.BRANCH_NAME"
        script.sh "mvn package"
    }

    def buildDockerImage(String imageName){
        script.echo "building the docker image"
        script.sh "docker build --tag $imageName ."
    }

    def dockerLogin(){
        script.withCredentials([script.usernamePassword(credentialsId:"dockerhub-credentials", usernameVariable: 'USR', passwordVariable: 'PWD')]) {
            script.sh "echo $script.PWD | docker login -u $script.USR --password-stdin"
        }
    }

    def dockerPush(String imageName){
        script.sh "docker push $imageName"
    }
}